'use strict';

module.exports = {
  client: {
  },
  server: {
    models: 'server/models/**/*.js',
    routes: 'server/routes/**/*.js'
  }
};
